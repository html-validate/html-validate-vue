import path from "path";
import { HtmlValidate, ConfigData, FileSystemConfigLoader } from "html-validate";
import "html-validate/jest";
import Plugin from "../src";

const config: ConfigData = {
	root: true,
	plugins: [Plugin],
	extends: ["html-validate:recommended", "html-validate-vue:recommended"],
	elements: ["html5", path.join(__dirname, "custom.json")],

	transform: {
		"\\.(vue|js)$": "html-validate-vue:html",
	},
};
const loader = new FileSystemConfigLoader(config);

describe("should handle <keep-alive> as transparent", () => {
	it.each`
		scenario                  | markup                                                   | expected
		${"flow in flow"}         | ${"<div><keep-alive><div></div></keep-alive></div>"}     | ${true}
		${"flow in phrasing"}     | ${"<span><keep-alive><div></div></keep-alive></span>"}   | ${false}
		${"phrasing in phrasing"} | ${"<span><keep-alive><span></span></keep-alive></span>"} | ${true}
		${"phrasing in flow"}     | ${"<div><keep-alive><span></span></keep-alive></div>"}   | ${true}
	`("$scenario", async ({ markup, expected }) => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(markup);
		expect(report.valid).toEqual(expected);
	});
});

describe("<portal>", () => {
	it("should be deprecated", async () => {
		expect.assertions(1);
		const markup = "<portal></portal>";
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(markup);
		expect(report).toHaveError(
			"deprecated",
			"<portal> is deprecated: the <portal> element has been renamed to <teleport>",
		);
	});
});

describe("should handle <router-link> as transparent", () => {
	it.each`
		scenario                  | markup                                                     | expected
		${"flow in flow"}         | ${"<div><router-link><div></div></router-link></div>"}     | ${true}
		${"flow in phrasing"}     | ${"<span><router-link><div></div></router-link></span>"}   | ${false}
		${"phrasing in phrasing"} | ${"<span><router-link><span></span></router-link></span>"} | ${true}
		${"phrasing in flow"}     | ${"<div><router-link><span></span></router-link></div>"}   | ${true}
	`("$scenario", async ({ markup, expected }) => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(markup);
		expect(report.valid).toEqual(expected);
	});
});

describe("<router-view>", () => {
	it("should be allowed under @flow", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString("<div><router-view></router-view></div>");
		expect(report).toBeValid();
	});

	it("should not be allowed under @phrasing", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString("<span><router-view></router-view></span>");
		expect(report).toBeInvalid();
	});

	it("should allow content", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const markup = /* HTML */ `
			<router-view v-slot="{ Component }">
				<component :is="Component" />
			</router-view>
			<router-view v-slot="{ Component }">
				<transition>
					<keep-alive>
						<component :is="Component" />
					</keep-alive>
				</transition>
			</router-view>
			<router-view v-slot="{ Component }">
				<div>
					<component :is="Component" />
				</div>
			</router-view>
		`;
		const report = await htmlvalidate.validateString(markup, "test.vue");
		expect(report).toBeValid();
	});

	it("should allow default slot", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const markup = /* HTML */ `
			<router-view>
				<template #default="{ Component }">
					<component :is="Component" />
				</template>
			</router-view>
		`;
		const report = await htmlvalidate.validateString(markup, "test.vue");
		expect(report).toBeValid();
	});
});

it("should allow <slot> when script-supporting elements is expected", async () => {
	expect.assertions(1);
	const htmlvalidate = new HtmlValidate(loader);
	const report = await htmlvalidate.validateString("<ul><slot></slot></ul>");
	expect(report).toBeValid();
});

describe("<suspense>", () => {
	describe("should handle as transparent", () => {
		it.each`
			scenario                  | markup                                               | expected
			${"flow in flow"}         | ${"<div><suspense><div></div></suspense></div>"}     | ${true}
			${"flow in phrasing"}     | ${"<span><suspense><div></div></suspense></span>"}   | ${false}
			${"phrasing in phrasing"} | ${"<span><suspense><span></span></suspense></span>"} | ${true}
			${"phrasing in flow"}     | ${"<div><suspense><span></span></suspense></div>"}   | ${true}
		`("$scenario", async ({ markup, expected }) => {
			expect.assertions(1);
			const htmlvalidate = new HtmlValidate(loader);
			const report = await htmlvalidate.validateString(markup);
			expect(report.valid).toEqual(expected);
		});
	});
});

describe("<teleport>", () => {
	it("should be allowed as content in phrasing", async () => {
		expect.assertions(1);
		const markup = "<span><teleport></teleport></span>";
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(markup);
		expect(report).toBeValid();
	});

	it("should allow flow as content", async () => {
		expect.assertions(1);
		const markup = "<span><teleport><div></div></teleport></span>";
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(markup);
		expect(report).toBeValid();
	});
});

describe("should handle <transition> as transparent", () => {
	it.each`
		scenario                  | markup                                                   | expected
		${"flow in flow"}         | ${"<div><transition><div></div></transition></div>"}     | ${true}
		${"flow in phrasing"}     | ${"<span><transition><div></div></transition></span>"}   | ${false}
		${"phrasing in phrasing"} | ${"<span><transition><span></span></transition></span>"} | ${true}
		${"phrasing in flow"}     | ${"<div><transition><span></span></transition></div>"}   | ${true}
	`("$scenario", async ({ markup, expected }) => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(markup);
		expect(report.valid).toEqual(expected);
	});
});

describe("<transition-group>", () => {
	describe("should handle as transparent", () => {
		it.each`
			scenario                  | markup                                                               | expected
			${"flow in flow"}         | ${"<div><transition-group><div></div></transition-group></div>"}     | ${true}
			${"flow in phrasing"}     | ${"<span><transition-group><div></div></transition-group></span>"}   | ${false}
			${"phrasing in phrasing"} | ${"<span><transition-group><span></span></transition-group></span>"} | ${true}
			${"phrasing in flow"}     | ${"<div><transition-group><span></span></transition-group></div>"}   | ${true}
		`("$scenario", async ({ markup, expected }) => {
			expect.assertions(1);
			const htmlvalidate = new HtmlValidate(loader);
			const report = await htmlvalidate.validateString(markup);
			expect(report.valid).toEqual(expected);
		});
	});

	it("should handle as given tagName when tag attribute is used", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `
			<div>
				<transition-group tag="span"><div></div></transition-group>
			</div>
			<span>
				<transition-group tag="span"><div></div></transition-group>
			</span>
		`;
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(markup, "my-file.vue");
		expect(report).toHaveErrors([
			{
				ruleId: "element-permitted-content",
				line: 3,
				message: "<div> element is not permitted as content under <span> (<transition-group>)",
			},
			{
				ruleId: "element-permitted-content",
				line: 6,
				message: "<div> element is not permitted as content under <span> (<transition-group>)",
			},
		]);
	});
});

describe("<component>", () => {
	it('should report error if missing "is" attribute', async () => {
		expect.assertions(2);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString("<component></component>");
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"element-required-attributes",
			'<component> is missing required "is" attribute',
		);
	});

	it('should load meta for tagname provided by "is"', async () => {
		expect.assertions(2);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(
			'<template>\n<component is="label"><div></div></component>\n</template>',
			"myfile.vue",
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"element-permitted-content",
			"<div> element is not permitted as content under <label> (<component>)",
		);
	});

	it('should handle unknown tagname in "is"', async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(
			'<template>\n<component is="missing"><div></div></component>\n</template>',
			"myfile.vue",
		);
		expect(report).toBeValid();
	});

	it("should handle :it", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(
			'<template>\n<component :is="tagname"><div></div></component>\n</template>',
			"myfile.vue",
		);
		expect(report).toBeValid();
	});

	it("should allow dynamic :is in @flow", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(
			'<template>\n<div><component :is="tagname"></component></div>\n</template>',
			"myfile.vue",
		);
		expect(report).toBeValid();
	});

	it("should allow dynamic :is in @phrasing", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(
			'<template>\n<span><component :is="tagname"></component></span>\n</template>',
			"myfile.vue",
		);
		expect(report).toBeValid();
	});

	it("should handle dynamic :is as transparent", async () => {
		expect.assertions(1);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(
			'<template>\n<span><component :is="tagname"><div></div></component></span>\n</template>',
			"myfile.vue",
		);
		expect(report).toHaveError(
			"element-permitted-content",
			"<div> element is not permitted as content under <span>",
		);
	});

	it('should load meta onto slot for tagname provided by "is" on component', async () => {
		expect.assertions(2);
		const htmlvalidate = new HtmlValidate(loader);
		const report = await htmlvalidate.validateString(
			'<template>\n<dynamic-component tagname="label"><template #foo><div></div></template></dynamic-component>\n</template>',
			"myfile.vue",
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"element-permitted-content",
			'<div> element is not permitted as content under slot "foo" (<label> via <dynamic-component>)',
		);
	});
});
