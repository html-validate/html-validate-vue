import fs from "node:fs";
import path from "node:path";
import { ConfigData, FileSystemConfigLoader, HtmlValidate, Message, Report } from "html-validate";
import "html-validate/jest";
import Plugin from "../src";

const config: ConfigData = {
	root: true,
	plugins: [Plugin],
	extends: ["html-validate:recommended", "html-validate-vue:recommended"],
	elements: ["html5", path.join(__dirname, "custom.json")],
	transform: {
		"^.*\\.vue$": "html-validate-vue",
	},
};
const loader = new FileSystemConfigLoader(config);

/**
 * Filter out properties not present in all supported versions of html-validate (see
 * peerDependencies). This required in the version matrix integration test.
 */
function filterReport(report: Report): void {
	for (const result of report.results) {
		for (const msg of result.messages) {
			const dst: Partial<Message & { ruleUrl: string }> = msg;
			delete dst.context;
			delete dst.ruleUrl;
			delete dst.selector;
		}
	}
}

function* findFiles(...dirs: string[]): Generator<string> {
	for (const dir of dirs) {
		for (const ent of fs.readdirSync(dir, { withFileTypes: true })) {
			const fullPath = path.join(dir, ent.name);
			if (ent.isDirectory()) {
				yield* findFiles(fullPath);
			} else if (ent.isFile()) {
				if (fullPath.endsWith(".vue")) {
					yield fullPath;
				}
			}
		}
	}
}

describe("smoketest", () => {
	const files = Array.from(findFiles("src", "test/smoketest"));
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate(loader);
	});

	it.each(files)("%s", async (filename) => {
		expect.assertions(2);
		const report = await htmlvalidate.validateFile(filename);
		filterReport(report);
		expect(report).toBeInvalid();
		expect(report.results[0].messages).toMatchSnapshot();
	});
});
