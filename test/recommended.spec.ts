import { ConfigData, FileSystemConfigLoader, HtmlValidate } from "html-validate";
import "html-validate/jest";
import Plugin from "../src";

const config: ConfigData = {
	root: true,
	plugins: [Plugin],
	extends: ["html-validate:recommended", "html-validate-vue:recommended"],
	elements: ["html5"],
};
const loader = new FileSystemConfigLoader(config);

let htmlvalidate: HtmlValidate;

beforeAll(() => {
	htmlvalidate = new HtmlValidate(loader);
});

it("should not report error for camelcase modifiers", async () => {
	expect.assertions(1);
	const report = await htmlvalidate.validateString("<div v-foo.barBaz></div>");
	expect(report).toBeValid();
});
