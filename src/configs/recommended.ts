import { type ConfigData } from "html-validate";
import vueElements from "../../elements.json";

export function createConfig(): ConfigData {
	const config: ConfigData = {
		elements: [vueElements],
		rules: {
			/* vue modifiers often use camelcase so allow by default */
			"attr-case": ["error", { style: "camelcase" }],

			/* self closing tags often used in vue code so allow by default*/
			"void-style": ["error", { style: "selfclose" }],
			"no-self-closing": "off",

			/* enable rules from this plugin */
			"vue/available-slots": "error",
			"vue/prefer-slot-shorthand": "error",
			"vue/required-slots": "error",
		},
	};

	return config;
}

export default createConfig();
