jest.mock("./html.transform", () => ({ transformHTML: jest.fn(() => []) }));
jest.mock("./js.transform", () => ({ transformJS: jest.fn(() => []) }));
jest.mock("./sfc.transform", () => ({ transformSFC: jest.fn(() => []) }));

import { Source } from "html-validate";
import { autodetect } from "./auto.transform";
import { transformHTML } from "./html.transform";
import { transformJS } from "./js.transform";
import { transformSFC } from "./sfc.transform";

function createSource(filename: string): Source {
	return {
		filename,
		data: "mock data",
		line: 1,
		column: 1,
		offset: 0,
	};
}

function transform(source: Source): void {
	/* consume generator, e.g. side-effects */
	Array.from(autodetect(source));
}

it("should select html transform for my-file.html", () => {
	expect.assertions(1);
	const source = createSource("my-file.html");
	transform(source);
	expect(transformHTML).toHaveBeenCalledWith(source);
});

it.each(["js", "jsx", "ts", "tsx"])(
	"should select js transform for my-file.%s",
	(extension: string) => {
		expect.assertions(1);
		const source = createSource(`my-file.${extension}`);
		transform(source);
		expect(transformJS).toHaveBeenCalledWith(source);
	},
);

it("should select sfc transform for my-file.vue", () => {
	expect.assertions(1);
	const source = createSource("my-file.vue");
	transform(source);
	expect(transformSFC).toHaveBeenCalledWith(source);
});
