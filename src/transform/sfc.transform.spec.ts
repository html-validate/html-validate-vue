import { transformFile } from "html-validate/test-utils";
import { transformSFC } from "./sfc.transform";

it("should extract template from single file component", async () => {
	expect.assertions(2);
	const result = await transformFile(transformSFC, "./test/component.vue");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 11,
		  "data": "
			<div>
				<!-- should handle dynamic attributes -->
				<img src="image.png" v-bind:alt="dynamicAlt">
				<img src="image.png" :alt="dynamicAlt">

				<!-- should handle @event -->
				<a @click="onClick">click me!</a>

				<!-- should not choke on boolean attributes -->
				<input type="text" required>

				<!-- should not choke on misc vue constructs -->
				<ul>
					<template v-for="item in items">
						<li>{{      }}</li>
					</template>
				</ul>

				<!-- should detect duplicated :class but not duplicate class + :class -->
				<p class="foo" :class="bar"></p>
				<p :class="foo" :class="bar"></p>

				<!-- should detect bindings and friends as having textual content -->
				<h1></h1>
				<h2 v-html="foo"></h2>
				<h3>{{     }}</h3>
				<h4><slot></slot></h4>

				<!-- should not allow omitted end tag on selfclosing element -->
				<hr>

				<!-- allow selfclosing element -->
				<hr />
				<div />

			</div>
		",
		  "filename": "./test/component.vue",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 10,
		  "originalData": "<template>
			<div>
				<!-- should handle dynamic attributes -->
				<img src="image.png" v-bind:alt="dynamicAlt">
				<img src="image.png" :alt="dynamicAlt">

				<!-- should handle @event -->
				<a @click="onClick">click me!</a>

				<!-- should not choke on boolean attributes -->
				<input type="text" required>

				<!-- should not choke on misc vue constructs -->
				<ul>
					<template v-for="item in items">
						<li>{{ item }}</li>
					</template>
				</ul>

				<!-- should detect duplicated :class but not duplicate class + :class -->
				<p class="foo" :class="bar"></p>
				<p :class="foo" :class="bar"></p>

				<!-- should detect bindings and friends as having textual content -->
				<h1></h1>
				<h2 v-html="foo"></h2>
				<h3>{{ foo }}</h3>
				<h4><slot></slot></h4>

				<!-- should not allow omitted end tag on selfclosing element -->
				<hr>

				<!-- allow selfclosing element -->
				<hr />
				<div />

			</div>
		</template>

		<style>
		 /* ... */
		</style>

		<script>
		/* ... */
		</script>
		",
		}
	`);
});

it("should ignore templates with preprocessors", async () => {
	expect.assertions(1);
	const result = await transformFile(transformSFC, "./test/lang.vue");
	expect(result).toHaveLength(0);
});

it("should handle missing template", async () => {
	expect.assertions(1);
	const result = await transformFile(transformSFC, "./test/missing.vue");
	expect(result).toHaveLength(0);
});
