import { type Source } from "html-validate";
import { TemplateExtractor } from "@html-validate/plugin-utils";
import { processAttribute, processElement } from "../hooks";

/**
 * Match template property in `Vue.component` call
 */
export function transformJS(source: Source): Iterable<Source> {
	const te = TemplateExtractor.fromString(source.data, source.filename);
	return Array.from(te.extractObjectProperty("template"), (cur) => {
		cur.originalData = source.originalData ?? source.data;
		cur.hooks = {
			processAttribute,
			processElement,
		};
		return cur;
	});
}

transformJS.api = 1;
