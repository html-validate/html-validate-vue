import { transformFile } from "html-validate/test-utils";
import { transformJS } from "./js.transform";

it("should extract templates from vue js component", async () => {
	expect.assertions(3);
	const result = await transformFile(transformJS, "./test/component.js");
	expect(result).toHaveLength(2);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 12,
		  "data": "<p>Lorem ipsum</p>",
		  "filename": "./test/component.js",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 4,
		  "offset": 60,
		  "originalData": "/* globals Vue:false */

		Vue.component("foo", {
			template: \`<p>Lorem ipsum</p>\`,
		});

		Vue.component("bar", {
			template: \`<i>Lorem <p>ipsum</p></i>\`,
		});
		",
		}
	`);
	expect(result[1]).toMatchInlineSnapshot(`
		{
		  "column": 12,
		  "data": "<i>Lorem <p>ipsum</p></i>",
		  "filename": "./test/component.js",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 8,
		  "offset": 121,
		  "originalData": "/* globals Vue:false */

		Vue.component("foo", {
			template: \`<p>Lorem ipsum</p>\`,
		});

		Vue.component("bar", {
			template: \`<i>Lorem <p>ipsum</p></i>\`,
		});
		",
		}
	`);
});
