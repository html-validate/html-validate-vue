import { transformFile } from "html-validate/test-utils";
import { transformHTML } from "./html.transform";

it("should apply hooks", async () => {
	expect.assertions(2);
	const result = await transformFile(transformHTML, "./test/markup.html");
	expect(result).toHaveLength(1);
	expect(result[0]).toMatchInlineSnapshot(`
		{
		  "column": 1,
		  "data": "<div :class="foo"></div>
		",
		  "filename": "./test/markup.html",
		  "hooks": {
		    "processAttribute": [Function],
		    "processElement": [Function],
		  },
		  "line": 1,
		  "offset": 0,
		}
	`);
});
