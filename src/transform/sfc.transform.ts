import { type Source } from "html-validate";
import { processAttribute, processElement } from "../hooks";
import { stripTemplating } from "../utils";

function findLocation(source: string, index: number, preamble: number): [number, number] {
	let line = 1;
	let prev = 0;
	let pos = source.indexOf("\n");
	while (pos !== -1) {
		if (pos > index) {
			return [line, index - prev + preamble + 1];
		}
		line++;
		prev = pos;
		pos = source.indexOf("\n", pos + 1);
	}
	return [line, 1];
}

/**
 * Match templates from SFC <template> tag
 */
export function* transformSFC(source: Source): Iterable<Source> {
	const sfc = /^(<template([^>]*)>)([^]*?)^<\/template>/gm;
	let match;
	while ((match = sfc.exec(source.data)) !== null) {
		const [, preamble, attr, data] = match;
		if (!/lang=".*?"/.exec(attr)) {
			const [line, column] = findLocation(source.data, match.index, preamble.length);
			yield {
				data: stripTemplating(data),
				filename: source.filename,
				line: line + (source.line - 1),
				column: column + (source.column - 1),
				offset: match.index + (source.offset || 0) + preamble.length,
				originalData: source.originalData ?? source.data,
				hooks: {
					processAttribute,
					processElement,
				},
			};
		}
	}
}

transformSFC.api = 1;
