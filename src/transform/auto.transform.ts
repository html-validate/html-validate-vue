import { type Source } from "html-validate";
import { transformHTML } from "./html.transform";
import { transformJS } from "./js.transform";
import { transformSFC } from "./sfc.transform";

/**
 * Returns true if the source is considered a SFC component
 */
function isVue(source: Source): boolean {
	return !!/\.vue$/i.exec(source.filename);
}

/**
 * Returns true if the source is considered javascript
 */
function isJavascript(source: Source): boolean {
	return !!/\.(jsx?|tsx?)$/i.exec(source.filename);
}

export function* autodetect(source: Source): Iterable<Source> {
	if (isVue(source)) {
		yield* transformSFC(source);
	} else if (isJavascript(source)) {
		yield* transformJS(source);
	} else {
		yield* transformHTML(source);
	}
}

autodetect.api = 1;
