import { type DOMReadyEvent, type HtmlElement, type RuleDocumentation, Rule } from "html-validate";
import { findUsedSlots } from "../utils";

declare module "html-validate" {
	interface MetaElement {
		requiredSlots?: string[];
	}
}

interface Context {
	element: string;
	slot: string;
	required: string[];
}

function difference<T>(a: Set<T>, b: Set<T>): Set<T> {
	const result = new Set(a);
	for (const elem of b) {
		result.delete(elem);
	}
	return result;
}

export class RequiredSlots extends Rule<Context> {
	public documentation(context?: Context): RuleDocumentation {
		if (context) {
			return {
				description: [
					`The <${context.element}> component requires slot "${context.slot}" to be implemented. Add \`<template v-slot:${context.slot}>\``,
					"",
					"The following slots are required",
				]
					.concat(context.required.map((slot) => `- ${slot}`))
					.join("\n"),
			};
		} else {
			return {
				description: "All required slots must be implemented.",
			};
		}
	}

	public setup(): void {
		this.on("dom:ready", (event: DOMReadyEvent) => {
			const doc = event.document;
			/* eslint-disable-next-line @typescript-eslint/no-deprecated -- need to
			 * use this for backwards compatibility until support for v7 is dropped */
			doc.visitDepthFirst((node: HtmlElement) => {
				/* ignore rule if element has no meta or meta does not specify available slots */
				const meta = node.meta;
				if (!meta?.requiredSlots) {
					return;
				}

				this.validateSlots(node, meta.requiredSlots);
			});
		});
	}

	private validateSlots(node: HtmlElement, requiredSlots: string[]): void {
		const usedSlots = findUsedSlots(node);
		const required = new Set(requiredSlots);
		const used = new Set(usedSlots.keys());
		const diff = difference(required, used);

		for (const missing of diff) {
			const context: Context = {
				element: node.tagName,
				slot: missing,
				required: requiredSlots,
			};
			this.report(
				node,
				`<${node.tagName}> component requires slot "${missing}" to be implemented`,
				null,
				context,
			);
		}
	}
}
