import { Rule, type RuleDocumentation, type AttributeEvent, DynamicValue } from "html-validate";

interface RuleContext {
	slot: string;
}

const prefix = "v-slot:";

function isVSlot(attr: string): string | null {
	if (!attr.startsWith(prefix)) {
		return null;
	}
	if (attr.length <= prefix.length) {
		return null;
	}
	const slot = attr.slice(prefix.length);
	if (/^\[.*\]$/.exec(slot)) {
		return null;
	}
	return slot;
}

function isDeprecatedSlot(attr: string, value: null | string | DynamicValue): string | null {
	if (attr !== "slot") {
		return null;
	}
	if (value instanceof DynamicValue) {
		return null;
	}
	if (value === null || value === "") {
		return null;
	}
	return value.toString();
}

export class PreferSlotShorthand extends Rule<RuleContext> {
	public documentation(context?: RuleContext): RuleDocumentation {
		const slot = context?.slot ?? "my-slot";
		return {
			description: [
				`Prefer to use \`#${slot}\` over \`v-slot:${slot}\`.`,
				"",
				"Vue.js supports a shorthand syntax for slots using `#` instead of `v-slot:` or the deprecated `slot` attribute, e.g. using `#header` instead of `v-slot:header`.",
			].join("\n"),
		};
	}

	public setup(): void {
		this.on("attr", (event: AttributeEvent) => {
			const { key: attr, value, target, keyLocation: location } = event;

			let slot: string | null;

			/* handle v-slot:xyz */
			slot = isVSlot(attr);
			if (slot) {
				const context: RuleContext = { slot };
				const message = `Prefer to use #${slot} over ${attr}`;
				this.report(target, message, location, context);
			}

			/* handle slot="xyz" */
			slot = isDeprecatedSlot(attr, value);
			if (slot) {
				const context: RuleContext = { slot };
				/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion -- value is checked by isDeprecatedSlot */
				const message = `Prefer to use #${slot} over ${attr}="${value!.toString()}"`;
				this.report(target, message, location, context);
			}
		});
	}
}
