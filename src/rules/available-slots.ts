import { type HtmlElement, Rule, type RuleDocumentation, type DOMReadyEvent } from "html-validate";
import { findUsedSlots } from "../utils";

declare module "html-validate" {
	interface MetaElement {
		slots?: string[];
	}
}

interface Context {
	element: string;
	slot: string;
	available: string[];
}

function difference<T>(a: Set<T>, b: Set<T>): Set<T> {
	const result = new Set(a);
	for (const elem of b) {
		result.delete(elem);
	}
	return result;
}

export class AvailableSlots extends Rule<Context> {
	public documentation(context?: Context): RuleDocumentation {
		if (context) {
			return {
				description: [
					`The <${context.element}> component does not have a slot named "${context.slot}". Only known slots can be specified.`,
					"",
					"The following slots are available",
				]
					.concat(context.available.map((slot) => `- ${slot}`))
					.join("\n"),
			};
		} else {
			return {
				description: "Only known slots can be specified.",
			};
		}
	}

	public setup(): void {
		this.on("dom:ready", (event: DOMReadyEvent) => {
			const doc = event.document;
			/* eslint-disable-next-line @typescript-eslint/no-deprecated -- need to
			 * use this for backwards compatibility until support for v7 is dropped */
			doc.visitDepthFirst((node: HtmlElement) => {
				/* ignore rule if element has no meta or meta does not specify available slots */
				const meta = node.meta;
				if (!meta?.slots) {
					return;
				}

				this.validateSlots(node, meta.slots);
			});
		});
	}

	private validateSlots(node: HtmlElement, slots: string[]): void {
		const usedSlots = findUsedSlots(node);
		const available = new Set(slots);
		const used = new Set(usedSlots.keys());
		const diff = difference(used, available);

		for (const missing of diff) {
			const context: Context = {
				element: node.tagName,
				slot: missing,
				available: slots,
			};
			this.report(
				node,
				`<${node.tagName}> component has no slot "${missing}"`,
				usedSlots.get(missing),
				context,
			);
		}
	}
}
