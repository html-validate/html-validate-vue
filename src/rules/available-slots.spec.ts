import { ConfigData, FileSystemConfigLoader, HtmlValidate } from "html-validate";
import "html-validate/jest";
import Plugin from "..";

const config: ConfigData = {
	root: true,
	plugins: [Plugin],
	elements: [
		{
			"my-component": {
				slots: ["foo", "with-dashes"],
			},
		},
	],
	rules: { "vue/available-slots": "error" },
};
const loader = new FileSystemConfigLoader(config);

describe("vue/available-slots", () => {
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate(loader);
	});

	it("should not report error when component uses no slots", async () => {
		expect.assertions(1);
		const report = await htmlvalidate.validateString("<my-component></my-component>");
		expect(report).toBeValid();
	});

	it("should not report error when component uses available slot", async () => {
		expect.assertions(1);
		const report = await htmlvalidate.validateString(
			"<my-component><template v-slot:foo></template></my-component>",
		);
		expect(report).toBeValid();
	});

	it("should not report error when component uses dynamic slot", async () => {
		expect.assertions(1);
		const report = await htmlvalidate.validateString(
			"<my-component><template v-slot:[foo]></template></my-component>",
		);
		expect(report).toBeValid();
	});

	it("should not report error for slot with dashes", async () => {
		expect.assertions(1);
		const report = await htmlvalidate.validateString(
			"<my-component><template v-slot:with-dashes></template></my-component>",
		);
		expect(report).toBeValid();
	});

	it("should report error when element has invalid attribute value (v-slot)", async () => {
		expect.assertions(2);
		const report = await htmlvalidate.validateString(
			"<my-component><template v-slot:bar></template></my-component>",
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError("vue/available-slots", '<my-component> component has no slot "bar"');
	});

	it("should report error when element has invalid attribute value (shorthand)", async () => {
		expect.assertions(2);
		const report = await htmlvalidate.validateString(
			"<my-component><template #bar></template></my-component>",
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError("vue/available-slots", '<my-component> component has no slot "bar"');
	});

	it("should report error when element has invalid attribute value (slot)", async () => {
		expect.assertions(2);
		const report = await htmlvalidate.validateString(
			'<my-component><template slot="bar"></template></my-component>',
		);
		expect(report).toBeInvalid();
		expect(report).toHaveError("vue/available-slots", '<my-component> component has no slot "bar"');
	});

	it("should contain documentation", async () => {
		expect.assertions(1);
		const context = {
			element: "my-component",
			slot: "missing-slot",
			available: ["foo", "bar", "baz"],
		};
		const config = loader.getConfigFor("inline");
		const docs = await htmlvalidate.getRuleDocumentation("vue/available-slots", config, context);
		expect(docs).toMatchSnapshot();
	});
});
