import { ConfigData, FileSystemConfigLoader, HtmlValidate } from "html-validate";
import "html-validate/jest";
import Plugin from "..";
import { processAttribute } from "../hooks";

const config: ConfigData = {
	root: true,
	plugins: [Plugin],
	rules: { "vue/prefer-slot-shorthand": "error" },
};
const loader = new FileSystemConfigLoader(config);

describe("vue/prefer-slot-shorthand", () => {
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate(loader);
	});

	it("should not report error when using shorthand", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template #foo></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should not report error for other attributes", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template id="foo"></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should not report error when using dynamic v-slot", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template v-slot:[dynamicSlotName]></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should not report error when using deprecated dynamic slot-slot", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template :slot="dynamicSlotName"></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should report error when using v-slot syntax", async () => {
		expect.assertions(2);
		const markup = /* HTML */ `<template v-slot:foobar></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/prefer-slot-shorthand",
			"Prefer to use #foobar over v-slot:foobar",
		);
	});

	it("should report error when using deprecated slot syntax", async () => {
		expect.assertions(2);
		const markup = /* HTML */ `<template slot="foobar"></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/prefer-slot-shorthand",
			'Prefer to use #foobar over slot="foobar"',
		);
	});

	it("should handle scope in shorthand", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template #foobar="{ scope }"></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should handle scope in v-slot", async () => {
		expect.assertions(2);
		const markup = /* HTML */ `<template v-slot:foobar="{ scope }"></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/prefer-slot-shorthand",
			"Prefer to use #foobar over v-slot:foobar",
		);
	});

	it("should handle incomplete v-slot", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template v-slot v-slot:></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should handle incomplete slot", async () => {
		expect.assertions(1);
		const markup = /* HTML */ `<template slot></template><template slot=""></template>`;
		const report = await htmlvalidate.validateString(markup, { processAttribute });
		expect(report).toBeValid();
	});

	it("should contain documentation", async () => {
		expect.assertions(1);
		const context = {
			slot: "foo",
		};
		const config = loader.getConfigFor("inline");
		const docs = await htmlvalidate.getRuleDocumentation(
			"vue/prefer-slot-shorthand",
			config,
			context,
		);
		expect(docs).toMatchSnapshot();
	});
});
