import { ConfigData, FileSystemConfigLoader, HtmlValidate } from "html-validate";
import "html-validate/jest";
import Plugin from "..";

const config: ConfigData = {
	root: true,
	plugins: [Plugin],
	elements: [
		{
			"my-component": {
				requiredSlots: ["foo"],
			},
		},
	],
	rules: { "vue/required-slots": "error" },
};
const loader = new FileSystemConfigLoader(config);

describe("vue/required-slots", () => {
	let htmlvalidate: HtmlValidate;

	beforeAll(() => {
		htmlvalidate = new HtmlValidate(loader);
	});

	it("should not report error when all required slots are implemented (v-slot)", async () => {
		expect.assertions(1);
		const report = await htmlvalidate.validateString(
			"<my-component><template v-slot:foo></template></my-component>",
		);
		expect(report).toBeValid();
	});

	it("should not report error when all required slots are implemented (shorthand)", async () => {
		expect.assertions(1);
		const report = await htmlvalidate.validateString(
			"<my-component><template #foo></template></my-component>",
		);
		expect(report).toBeValid();
	});

	it("should not report error when all required slots are implemented (slot)", async () => {
		expect.assertions(1);
		const report = await htmlvalidate.validateString(
			'<my-component><template slot="foo"></template></my-component>',
		);
		expect(report).toBeValid();
	});

	it("should report error when component is missing a required slot", async () => {
		expect.assertions(2);
		const report = await htmlvalidate.validateString("<my-component></my-component>");
		expect(report).toBeInvalid();
		expect(report).toHaveError(
			"vue/required-slots",
			'<my-component> component requires slot "foo" to be implemented',
		);
	});

	it("should contain documentation", async () => {
		expect.assertions(1);
		const context = {
			element: "my-component",
			slot: "missing-slot",
			required: ["foo", "bar", "baz"],
		};
		const config = loader.getConfigFor("inline");
		const docs = await htmlvalidate.getRuleDocumentation("vue/required-slots", config, context);
		expect(docs).toMatchSnapshot();
	});
});
