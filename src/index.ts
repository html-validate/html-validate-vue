import { type Plugin, compatibilityCheck } from "html-validate";
import { name, peerDependencies } from "../package.json";
import configs from "./configs";
import rules from "./rules";
import transformer from "./transform";
import elementSchema from "./schema.json";

const range = peerDependencies["html-validate"];
compatibilityCheck(name, range);

const plugin: Plugin = {
	name,
	configs,
	rules,
	transformer,
	elementSchema,
};

export default plugin;
