import { DynamicValue, type AttributeData } from "html-validate";

export function* processAttribute(attr: AttributeData): Iterable<AttributeData> {
	/* always yield original attribute */
	yield attr;

	/* setup :foo and v-bind:foo alias */
	const bind = /^(?:v-bind)?:(.*)$/.exec(attr.key);
	if (bind) {
		yield {
			...attr,
			key: bind[1],
			value: new DynamicValue(attr.value ? String(attr.value) : ""),
			originalAttribute: attr.key,
		};
	}
}
