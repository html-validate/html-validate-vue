import { AttributeData, DynamicValue, HtmlElement, Source, Config, Parser } from "html-validate";
import { processAttribute as processAttributeGenerator } from "./attribute";

function processAttribute(attr: AttributeData): AttributeData[] {
	return Array.from(processAttributeGenerator(attr));
}

it("should handle : as target attribute", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: ":foo",
		value: "bar",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: ":foo",
			value: "bar",
			quote: "'",
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			originalAttribute: ":foo",
			quote: "'",
		},
	]);
});

it("should handle v-bind: as target attribute", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "v-bind:foo",
		value: "bar",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "v-bind:foo",
			value: "bar",
			quote: "'",
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			originalAttribute: "v-bind:foo",
			quote: "'",
		},
	]);
});

it("should handle shorthand :foo attribute", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: ":foo",
		value: null,
		quote: null,
	});
	expect(attrs).toEqual([
		{
			key: ":foo",
			value: null,
			quote: null,
		},
		{
			key: "foo",
			value: expect.any(DynamicValue),
			originalAttribute: ":foo",
			quote: null,
		},
	]);
});

it("should leave other attributes intact", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "foo",
		value: "bar",
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "foo",
			value: "bar",
			quote: "'",
		},
	]);
});

it("should handle boolean attributes", () => {
	expect.assertions(1);
	const attrs = processAttribute({
		key: "foo",
		value: null,
		quote: "'",
	});
	expect(attrs).toEqual([
		{
			key: "foo",
			value: null,
			quote: "'",
		},
	]);
});

it("querySelector should find class with :class present", async () => {
	expect.assertions(3);
	const config = Config.empty();
	const resolved = await config.resolve();
	const parser = new Parser(resolved);
	const source: Source = {
		data: '<p class="foo" :class="bar"></p>',
		filename: "inline",
		line: 1,
		column: 1,
		offset: 0,
		hooks: {
			processAttribute,
		},
	};
	const doc = parser.parseHtml(source);
	const node = doc.querySelector(".foo")!;
	expect(node).toBeInstanceOf(HtmlElement);
	expect(Array.from(node.classList)).toEqual(["foo"]);
	expect(node.getAttribute("class", true)).toEqual([
		expect.objectContaining({
			key: "class",
			value: "foo",
		}),
		expect.objectContaining({
			key: "class",
			value: expect.any(DynamicValue),
			originalAttribute: ":class",
		}),
	]);
});
