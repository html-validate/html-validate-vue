import {
	DynamicValue,
	type HtmlElement,
	type MetaElement,
	type ProcessElementContext,
} from "html-validate";
import { findSlotAttribute } from "../utils";

declare module "html-validate" {
	export interface MetaData {
		component?: string;
	}
}

function isBound(node: HtmlElement): boolean {
	return node.hasAttribute("v-html");
}

function isSlot(node: HtmlElement): boolean {
	return node.is("slot");
}

function loadSlotMeta(context: ProcessElementContext, slotname: string, node: HtmlElement): void {
	const parent = node.parent;
	if (!parent) {
		return;
	}

	/* Generate a new virtual tagname to load meta element from */
	const getMetaFor = (tagName: string): MetaElement | null => {
		const meta = context.getMetaFor(tagName);
		return meta && meta.tagName !== "*" ? meta : null;
	};
	const key = `${parent.tagName}#${slotname}`;
	const fallbackKey = `${parent.tagName}:${slotname}`;
	const meta = getMetaFor(key) ?? getMetaFor(fallbackKey);
	if (!meta) {
		return;
	}

	node.loadMeta(meta);

	/* add an annotation so error messages will be more understandable */
	node.setAnnotation(`slot "${slotname}" (<${parent.tagName}>)`);
}

/**
 * Handle <component> or a component with a slot wrapped in it.
 */
function loadComponentMeta(
	context: ProcessElementContext,
	node: HtmlElement,
	component: string,
): void {
	let src: HtmlElement = node;

	/* if this is a slot with wrapped in a <component> we fetch attributes from
	 * the parent element, i.e. the actual component */
	if (node.tagName === "template" && src.parent) {
		src = src.parent;
	}

	/* try to fetch the attribute containing the element to use */
	const attr = src.getAttribute(component);
	if (!attr || attr.isDynamic) {
		return;
	}

	/* use the attribute value to lookup new metadata based on the tagname */
	const tagName = attr.value as string;
	const meta = context.getMetaFor(tagName);
	if (!meta || meta.tagName === "*") {
		return;
	}

	node.loadMeta(meta);

	/* update or add new annotation so error messages will be more descriptive */
	if (node.isSameNode(src)) {
		node.setAnnotation(`<${meta.tagName}> (<${node.tagName}>)`);
	} else {
		const name = node.annotatedName.replace("(<", `(<${meta.tagName}> via <`);
		node.setAnnotation(name);
	}
}

export function processElement(this: ProcessElementContext, node: HtmlElement): void {
	const slot = findSlotAttribute(node);

	/* If this element is a slot we load new metadata properties onto the element
	 * to support adding properties directly to slots, e.g. to allow/disallow
	 * content in the slot. */
	if (slot) {
		const [slotname] = slot;
		loadSlotMeta(this, slotname, node);
	}

	/* handle usage of <component>, either directly or inside a component */
	if (node.meta?.component) {
		loadComponentMeta(this, node, node.meta.component);
	}

	/* Mark as having dynamic text if the element either content bound via v-html
	 * or if it is a slot. This is needed for rules with requires textual content,
	 * e.g a heading cannot be empty. */
	if (isBound(node) || isSlot(node)) {
		node.appendText(new DynamicValue(""), node.location);
	}
}
