import {
	type Location,
	type MetaDataTable,
	type ProcessElementContext,
	Config,
	DynamicValue,
	MetaCopyableProperty,
	Parser,
	TextNode,
} from "html-validate";
import html5 from "html-validate/elements/html5";
import elements from "../../elements.json";
import schema from "../schema.json";
import { processElement } from "./element";

declare module "html-validate" {
	interface MetaData {
		mock?: string;
	}
}

/* manually mark "mock" and "component" as a copyable property as we don't
 * initialize the full plugin. */
MetaCopyableProperty.push("mock", "component");

async function mockContext(
	obj: MetaDataTable = {},
): Promise<{ context: ProcessElementContext; parser: Parser }> {
	const config = await Config.fromObject([], {
		elements: [html5, elements, obj],
		plugins: [
			{
				elementSchema: {
					properties: {
						mock: {
							type: "string",
							copyable: true,
						},
						...schema.properties,
					},
				},
			},
		],
	});
	const resolved = await config.resolve();
	const table = resolved.getMetaTable();
	const parser = new Parser(resolved);
	return {
		context: {
			getMetaFor: jest.fn((tagName: string) => table.getMetaFor(tagName)),
		},
		parser,
	};
}

describe("slot metadata", () => {
	it("should load slot metadata with # delimiter", async () => {
		expect.assertions(2);
		const { context, parser } = await mockContext({
			"parent#slot-name": {
				mock: "with-hash-delimiter",
			},
		});
		const markup = /* HTML */ `
			<parent>
				<template v-slot:slot-name></template>
			</parent>
		`;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("template")!;
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("parent#slot-name");
		expect(node.meta).toEqual(expect.objectContaining({ mock: "with-hash-delimiter" }));
	});

	it("should load slot metadata with : delimiter as fallback", async () => {
		expect.assertions(2);
		const { context, parser } = await mockContext({
			"parent:slot-name": {
				mock: "with-colon-delimiter",
			},
		});
		const markup = /* HTML */ `
			<parent>
				<template v-slot:slot-name></template>
			</parent>
		`;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("template")!;
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("parent:slot-name");
		expect(node.meta).toEqual(expect.objectContaining({ mock: "with-colon-delimiter" }));
	});

	it("should handle when slot is missing parent", async () => {
		expect.assertions(1);
		const { context, parser } = await mockContext();
		const markup = /* HTML */ ` <template v-slot:slot-name></template> `;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("template")!;
		expect(() => {
			processElement.call(context, node);
		}).not.toThrow();
	});

	it("should handle when slot is missing metadata", async () => {
		expect.assertions(1);
		const { context, parser } = await mockContext();
		const markup = /* HTML */ `
			<parent>
				<template v-slot:slot-name></template>
			</parent>
		`;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("template")!;
		expect(() => {
			processElement.call(context, node);
		}).not.toThrow();
	});
});

describe("dynamic component", () => {
	it('should load meta for tagname provided by "is"', async () => {
		expect.assertions(3);
		const { context, parser } = await mockContext({
			div: {
				mock: "div",
			},
		});
		const markup = /* HTML */ ` <component is="div"></component> `;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("component")!;
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("div");
		expect(node.meta).toEqual(
			expect.objectContaining({
				tagName: "component",
				mock: "div",
			}),
		);
		expect(node.annotatedName).toBe("<div> (<component>)");
	});

	it('should handle unknown tagname in "is"', async () => {
		expect.assertions(3);
		const { context, parser } = await mockContext();
		const markup = /* HTML */ ` <component is="unknown"></component> `;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("component")!;
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("unknown");
		expect(node.meta).toEqual(
			expect.objectContaining({
				tagName: "component",
			}),
		);
		expect(node.annotatedName).toBe("<component>");
	});

	it('should handle dynamic value for "it"', async () => {
		expect.assertions(3);
		const location: Location = {
			filename: "mock-file",
			line: 1,
			column: 1,
			offset: 0,
			size: 1,
		};
		const { context, parser } = await mockContext();
		const markup = /* HTML */ ` <component></component> `;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("component")!;
		node.setAttribute("is", new DynamicValue("prop"), location, location);
		processElement.call(context, node);
		expect(context.getMetaFor).not.toHaveBeenCalled();
		expect(node.meta).toEqual(
			expect.objectContaining({
				tagName: "component",
			}),
		);
		expect(node.annotatedName).toBe("<component>");
	});

	it("should load meta for slot provided by component attribute", async () => {
		expect.assertions(4);
		const { context, parser } = await mockContext({
			div: {
				mock: "div",
			},
			"mock-element:foo": {
				component: "wrap",
			},
		});
		const markup = /* HTML */ `
			<mock-element wrap="div">
				<template v-slot:foo></template>
			</mock-element>
		`;
		const document = parser.parseHtml(markup);
		const node = document.querySelector("template")!;
		processElement.call(context, node);
		expect(context.getMetaFor).toHaveBeenCalledWith("mock-element:foo");
		expect(context.getMetaFor).toHaveBeenCalledWith("div");
		expect(node.meta).toEqual(
			expect.objectContaining({
				tagName: "template",
				mock: "div",
			}),
		);
		expect(node.annotatedName).toBe(`slot "foo" (<div> via <mock-element>)`);
	});
});

it("should add dynamic text when v-html is present", async () => {
	expect.assertions(1);
	const { context, parser } = await mockContext();
	const markup = /* HTML */ ` <p v-html="bar"></p> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("p")!;
	processElement.call(context, node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should add dynamic text when node is <slot>", async () => {
	expect.assertions(1);
	const { context, parser } = await mockContext();
	const markup = /* HTML */ ` <slot></slot> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("slot")!;
	processElement.call(context, node);
	expect(node.childNodes).toEqual([expect.any(TextNode)]);
});

it("should not add dynamic text if no dynamic content attribute is present", async () => {
	expect.assertions(1);
	const { context, parser } = await mockContext();
	const markup = /* HTML */ ` <p></p> `;
	const document = parser.parseHtml(markup);
	const node = document.querySelector("p")!;
	processElement.call(context, node);
	expect(node.childNodes).toEqual([]);
});
