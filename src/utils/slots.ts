import { type HtmlElement, type Location } from "html-validate";

const slotAttr = /(?:v-slot:|#)([^[]+)/;

function haveSlot(value: [string, Location] | null): value is [string, Location] {
	return value !== null;
}

/**
 * Search all children for slots and return both slot names and locations.
 */
export function findUsedSlots(node: HtmlElement): Map<string, Location> {
	const template = node.querySelectorAll(">template, >[slot]");
	const slots = template.map((child) => findSlotAttribute(child)).filter(haveSlot);
	const initial = new Map<string, Location>();
	return slots.reduce((result: Map<string, Location>, [key, val]) => {
		result.set(key, val);
		return result;
	}, initial);
}

/**
 * Find the name of the slot and its location used by an element. Searches
 * both v-slot:foo and slot="foo".
 */
export function findSlotAttribute(node: HtmlElement): [string, Location] | null {
	for (const attr of node.attributes) {
		/* find v-slot directive */
		const match = slotAttr.exec(attr.key);
		if (match) {
			const [, slot] = match;
			return [slot, attr.keyLocation];
		}

		/* find deprecated slot attribute */
		if (attr.key === "slot" && typeof attr.value === "string") {
			/* eslint-disable-next-line @typescript-eslint/no-non-null-assertion -- valueLocation will always be set the value is present */
			return [attr.value.toString(), attr.valueLocation!];
		}
	}
	return null;
}
