# html-validate-vue changelog

## 7.1.5 (2025-02-09)

### Bug Fixes

- **deps:** update dependency semver to v7.7.1 ([50c310f](https://gitlab.com/html-validate/html-validate-vue/commit/50c310f5b5f6b571fa7c98ced18772a9c03d3534))

## 7.1.4 (2025-02-02)

### Bug Fixes

- **deps:** update dependency semver to v7.7.0 ([fa28ab1](https://gitlab.com/html-validate/html-validate-vue/commit/fa28ab168484254a1e2a24305295786c0847cafb))

## 7.1.3 (2024-12-29)

### Bug Fixes

- **deps:** pin dependency @html-validate/plugin-utils to 2.0.3 ([b95382c](https://gitlab.com/html-validate/html-validate-vue/commit/b95382c80dfb135a32f0bfa24bede3d743a821c5))

## 7.1.2 (2024-12-23)

### Bug Fixes

- **deps:** update @html-validate/plugin-utils to v2.0.3 ([6a371c0](https://gitlab.com/html-validate/html-validate-vue/commit/6a371c0d80efac4d4dee6da2f493b51405bdf3b2))

## 7.1.1 (2024-12-22)

### Bug Fixes

- **deps:** pin dependency @html-validate/plugin-utils to 2.0.2 ([73971c3](https://gitlab.com/html-validate/html-validate-vue/commit/73971c342144c476260995b96132edccb381ec47))

## 7.1.0 (2024-12-21)

### Features

- esm/cjs hybrid build ([52ed3c4](https://gitlab.com/html-validate/html-validate-vue/commit/52ed3c4f92aad4e1dffc6e3d20b31ed0da0116ce))

## 7.0.0 (2024-12-15)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v18 or later
- **deps:** require html-validate v8 or later

### Features

- **deps:** require html-validate v8 or later ([a7874ae](https://gitlab.com/html-validate/html-validate-vue/commit/a7874ae2f34049d676dfae75ed65678093c88a17))
- **deps:** require nodejs v18 or later ([7135759](https://gitlab.com/html-validate/html-validate-vue/commit/7135759c1db380640ce80a927a81351df5049d7d))
- **deps:** support html-validate v9 ([9109405](https://gitlab.com/html-validate/html-validate-vue/commit/91094053631da96efeb6ad8d6bd0f84b45c48aae))
- **deps:** update dependency @html-validate/plugin-utils to v2 ([741fc4c](https://gitlab.com/html-validate/html-validate-vue/commit/741fc4cd4f4f2147819e997edb449885af3dec1a))

## 6.2.0 (2024-11-24)

### Features

- remove usage of filesystem (`node:fs`) ([b262c3a](https://gitlab.com/html-validate/html-validate-vue/commit/b262c3a6993d37ff9d5d2d4fc4c655f930b95bf2))

## 6.1.0 (2024-09-24)

### Features

- handle same-name prop shorthand ([7764d1e](https://gitlab.com/html-validate/html-validate-vue/commit/7764d1e27a91d5a6ec382ce89c6c8cbf3de06e36))

## 6.0.8 (2024-07-21)

### Bug Fixes

- **deps:** update dependency semver to v7.6.3 ([36f8d71](https://gitlab.com/html-validate/html-validate-vue/commit/36f8d714937c6d8a64d5b49d55c7b788568d4099))

## 6.0.7 (2024-05-12)

### Bug Fixes

- **deps:** update dependency semver to v7.6.1 ([8d4b631](https://gitlab.com/html-validate/html-validate-vue/commit/8d4b631e931054a864f06a6005ed97c8c60482d5))
- **deps:** update dependency semver to v7.6.2 ([b2bf989](https://gitlab.com/html-validate/html-validate-vue/commit/b2bf989b5466fee25182a489eda96daddf2469de))

## 6.0.6 (2024-04-05)

### Bug Fixes

- **router-view:** allow content under `<router-view>` ([c11b3cc](https://gitlab.com/html-validate/html-validate-vue/commit/c11b3cc3ad2807303e03411b2146e781a30a4eb5)), closes [#12](https://gitlab.com/html-validate/html-validate-vue/issues/12)

## [6.0.5](https://gitlab.com/html-validate/html-validate-vue/compare/v6.0.4...v6.0.5) (2024-2-11)

### Bug Fixes

- **deps:** update dependency semver to v7.6.0 ([1d88ead](https://gitlab.com/html-validate/html-validate-vue/commit/1d88ead3b70dac38d8cd241c314210458926a47d))

## [6.0.4](https://gitlab.com/html-validate/html-validate-vue/compare/v6.0.3...v6.0.4) (2023-07-16)

### Bug Fixes

- drop support for deprecated `void` rule ([e3a4cc6](https://gitlab.com/html-validate/html-validate-vue/commit/e3a4cc6fcc25e29f4d39d7319ea9c31d9ce9d692))

## [6.0.3](https://gitlab.com/html-validate/html-validate-vue/compare/v6.0.2...v6.0.3) (2023-07-09)

### Dependency upgrades

- **deps:** update dependency semver to v7.5.4 ([e801c80](https://gitlab.com/html-validate/html-validate-vue/commit/e801c80fd0c246dd55b9408ccab3c770af0943b9))

## [6.0.2](https://gitlab.com/html-validate/html-validate-vue/compare/v6.0.1...v6.0.2) (2023-06-25)

### Dependency upgrades

- **deps:** update dependency semver to v7.5.3 ([a3c655d](https://gitlab.com/html-validate/html-validate-vue/commit/a3c655da404095c138a6462a04649c56c06b808e))

## [6.0.1](https://gitlab.com/html-validate/html-validate-vue/compare/v6.0.0...v6.0.1) (2023-06-18)

### Dependency upgrades

- **deps:** update dependency semver to v7.5.2 ([46a2d9f](https://gitlab.com/html-validate/html-validate-vue/commit/46a2d9f8c6f0d56f4929b98475382a881e011e18))

## [6.0.0](https://gitlab.com/html-validate/html-validate-vue/compare/v5.1.4...v6.0.0) (2023-06-04)

### ⚠ BREAKING CHANGES

- **deps:** require html-validate v5 or later
- **deps:** require nodejs v16 or later

### Features

- **deps:** require html-validate v5 or later ([3423114](https://gitlab.com/html-validate/html-validate-vue/commit/34231143743ae2a860b4efe892d39a4396f90718))
- **deps:** require nodejs v16 or later ([0ab86ce](https://gitlab.com/html-validate/html-validate-vue/commit/0ab86ce390a92bef64c27e6a01753914285e4cff))

### Dependency upgrades

- **deps:** pin dependency @html-validate/plugin-utils to 1.0.1 ([2a9da0a](https://gitlab.com/html-validate/html-validate-vue/commit/2a9da0a0c2da96e338fe2f227008030b3f4be05c))
- **deps:** support html-validate v8 ([b4f17a8](https://gitlab.com/html-validate/html-validate-vue/commit/b4f17a8919d7673f15cd05a4d560ca2921243ae7))
- **deps:** support html-validate v8 ([8abf789](https://gitlab.com/html-validate/html-validate-vue/commit/8abf789308edcdb2db5fc7e75409aa7028cf44ea))
- **deps:** update dependency @html-validate/plugin-utils to v1.0.2 ([e9cb34e](https://gitlab.com/html-validate/html-validate-vue/commit/e9cb34e4fbc7b793b93c39e5df795dcb1b2c0ffe))

## [5.1.4](https://gitlab.com/html-validate/html-validate-vue/compare/v5.1.3...v5.1.4) (2023-05-14)

### Dependency upgrades

- **deps:** update dependency semver to v7.5.1 ([de19fa6](https://gitlab.com/html-validate/html-validate-vue/commit/de19fa6d61cee37f571c2ae5feb58dd90de5bb68))

## [5.1.3](https://gitlab.com/html-validate/html-validate-vue/compare/v5.1.2...v5.1.3) (2023-04-23)

### Dependency upgrades

- **deps:** update dependency semver to v7.5.0 ([e4718d4](https://gitlab.com/html-validate/html-validate-vue/commit/e4718d43a542549fcb96e4421f27f5a75e3f3d51))

## [5.1.2](https://gitlab.com/html-validate/html-validate-vue/compare/v5.1.1...v5.1.2) (2023-04-16)

### Dependency upgrades

- **deps:** update dependency semver to v7.4.0 ([d796154](https://gitlab.com/html-validate/html-validate-vue/commit/d796154f60a0169255cd7dc389080549bd0e1bc2))

## [5.1.1](https://gitlab.com/html-validate/html-validate-vue/compare/v5.1.0...v5.1.1) (2022-10-09)

### Dependency upgrades

- **deps:** update dependency semver to v7.3.8 ([bd161c7](https://gitlab.com/html-validate/html-validate-vue/commit/bd161c7f792cbe7246cb39450a078a5aac6e3679))

## [5.1.0](https://gitlab.com/html-validate/html-validate-vue/compare/v5.0.0...v5.1.0) (2022-05-23)

### Features

- support `<portal>` ([50f0023](https://gitlab.com/html-validate/html-validate-vue/commit/50f0023f64cbe87ad3346b3a59ae0abc41a708d8))
- support `<suspense>` ([e2c3f4e](https://gitlab.com/html-validate/html-validate-vue/commit/e2c3f4efda8f91988e2a01f04d21130654b3484c))
- support `<teleport>` ([1444ad0](https://gitlab.com/html-validate/html-validate-vue/commit/1444ad0d386f88a5f81c065546ec37ba2c55d505)), closes [#11](https://gitlab.com/html-validate/html-validate-vue/issues/11)
- support `<transition-group>` ([9c0d018](https://gitlab.com/html-validate/html-validate-vue/commit/9c0d018780aee2c50ee1e2d545306951e7cd32a2))

## [5.0.0](https://gitlab.com/html-validate/html-validate-vue/compare/v4.3.0...v5.0.0) (2022-05-08)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([2be6883](https://gitlab.com/html-validate/html-validate-vue/commit/2be688399d4b52d99762df28698cbce944b68830))

### Dependency upgrades

- **deps:** support html-validate v7 ([53883ae](https://gitlab.com/html-validate/html-validate-vue/commit/53883aef298f309eb95e6887ed55c69adcc99cb6))

## [4.3.0](https://gitlab.com/html-validate/html-validate-vue/compare/v4.2.4...v4.3.0) (2022-05-08)

### Features

- require html-validate v4.14 or later ([58de4db](https://gitlab.com/html-validate/html-validate-vue/commit/58de4db1ee624afa22fcb95e21cc3f77d87d7e9b))

### [4.2.4](https://gitlab.com/html-validate/html-validate-vue/compare/v4.2.3...v4.2.4) (2022-04-23)

### Bug Fixes

- fix `semver` dependency being extraneous pulled in as dependency ([7489397](https://gitlab.com/html-validate/html-validate-vue/commit/74893972a13c3b52172c113ac1cc6a27809e1dec))

### [4.2.3](https://gitlab.com/html-validate/html-validate-vue/compare/v4.2.2...v4.2.3) (2022-04-17)

### Dependency upgrades

- **deps:** update dependency semver to v7.3.7 ([324e59f](https://gitlab.com/html-validate/html-validate-vue/commit/324e59f16fc5c169010bfbc94bfa8ad3d89c90d8))

### [4.2.2](https://gitlab.com/html-validate/html-validate-vue/compare/v4.2.1...v4.2.2) (2022-04-10)

### Dependency upgrades

- **deps:** update dependency semver to v7.3.6 ([8d97ffd](https://gitlab.com/html-validate/html-validate-vue/commit/8d97ffd54816493bdf05c20f8b4c991f94c33ece))

### [4.2.1](https://gitlab.com/html-validate/html-validate-vue/compare/v4.2.0...v4.2.1) (2022-03-20)

### Bug Fixes

- ignore dynamic slots in `prefer-slot-shorthand` ([903eebc](https://gitlab.com/html-validate/html-validate-vue/commit/903eebca25ef73296ce0ea5265511fa1ee529350))

## [4.2.0](https://gitlab.com/html-validate/html-validate-vue/compare/v4.1.0...v4.2.0) (2022-02-20)

### Features

- new rule `prefer-slot-shorthand` ([c67fbc4](https://gitlab.com/html-validate/html-validate-vue/commit/c67fbc461a03f80523f628aa48cee3324b266ea8)), closes [#7](https://gitlab.com/html-validate/html-validate-vue/issues/7)

## [4.1.0](https://gitlab.com/html-validate/html-validate-vue/compare/v4.0.3...v4.1.0) (2022-02-20)

### Features

- bundle dependencies ([cafeb08](https://gitlab.com/html-validate/html-validate-vue/commit/cafeb089a9faa89d298798b38616d209a0a53916))

### Dependency upgrades

- **deps:** pin dependency semver to 7.3.5 ([91ba594](https://gitlab.com/html-validate/html-validate-vue/commit/91ba594d858a0995ab31af7b74a79c9c12e94664))

### [4.0.3](https://gitlab.com/html-validate/html-validate-vue/compare/v4.0.2...v4.0.3) (2022-02-05)

### Bug Fixes

- html-validate v6.1 compatibility ([9344375](https://gitlab.com/html-validate/html-validate-vue/commit/9344375fad1c67511479cc048113f266bdcfb80a))

### [4.0.2](https://gitlab.com/html-validate/html-validate-vue/compare/v4.0.1...v4.0.2) (2021-09-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v6 ([fd33619](https://gitlab.com/html-validate/html-validate-vue/commit/fd336193a8c8fb56844b5e01633399bede66e24a))

### [4.0.1](https://gitlab.com/html-validate/html-validate-vue/compare/v4.0.0...v4.0.1) (2021-06-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v5 ([1ca7122](https://gitlab.com/html-validate/html-validate-vue/commit/1ca7122f771a4a6d828db07171f23de462fb6d14))

## [4.0.0](https://gitlab.com/html-validate/html-validate-vue/compare/v3.2.1...v4.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([4c1e760](https://gitlab.com/html-validate/html-validate-vue/commit/4c1e760221523e2d61e6e49b37ba87be3f3c25cf))

### [3.2.1](https://gitlab.com/html-validate/html-validate-vue/compare/v3.2.0...v3.2.1) (2021-03-21)

### Dependency upgrades

- **deps:** accept any v7 version of `semver` ([c2a3462](https://gitlab.com/html-validate/html-validate-vue/commit/c2a3462a0e1f662cefe5dd6a22a5da5beddfdaaa))

## [3.2.0](https://gitlab.com/html-validate/html-validate-vue/compare/v3.1.2...v3.2.0) (2020-11-08)

### Features

- `html-validate@4.0.0` compatibility ([98edbd7](https://gitlab.com/html-validate/html-validate-vue/commit/98edbd73104718d5eda8bae470807aef91af5d22))
- compatibility with upcoming html-validate@4.0.0 ([4b0c880](https://gitlab.com/html-validate/html-validate-vue/commit/4b0c880191e98c22f1572e9b5bb96cc49338e725))

### Bug Fixes

- bump `peerDependency` and `engines` requirements ([4a98cdb](https://gitlab.com/html-validate/html-validate-vue/commit/4a98cdb86aa830dfb1896ee0d9050f8ab2966e4a))
- migrate to `dist` folder ([d64e467](https://gitlab.com/html-validate/html-validate-vue/commit/d64e4676547b37c1fe106636ee2810be13b4c8d6))

## [3.1.2](https://gitlab.com/html-validate/html-validate-vue/compare/v3.1.1...v3.1.2) (2020-11-01)

## [3.1.1](https://gitlab.com/html-validate/html-validate-vue/compare/v3.1.0...v3.1.1) (2020-10-25)

# [3.1.0](https://gitlab.com/html-validate/html-validate-vue/compare/v3.0.3...v3.1.0) (2020-04-05)

### Features

- `#` slot delimiter (fixes [#3](https://gitlab.com/html-validate/html-validate-vue/issues/3)) ([319ca49](https://gitlab.com/html-validate/html-validate-vue/commit/319ca490b8f1b1666792b9633f14ce3ca960e5e3))

## [3.0.3](https://gitlab.com/html-validate/html-validate-vue/compare/v3.0.2...v3.0.3) (2020-03-29)

## [3.0.2](https://gitlab.com/html-validate/html-validate-vue/compare/v3.0.1...v3.0.2) (2020-02-17)

### Bug Fixes

- **config:** use new void rules ([c622cbc](https://gitlab.com/html-validate/html-validate-vue/commit/c622cbc8f48379e3534a26571175e697acc15af0))

## [3.0.1](https://gitlab.com/html-validate/html-validate-vue/compare/v3.0.0...v3.0.1) (2020-02-12)

### Bug Fixes

- fix `<component>` not being allowed anywhere ([20a1eba](https://gitlab.com/html-validate/html-validate-vue/commit/20a1eba56221f11d618241b3880ef09508e8f3ec))

# [3.0.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.6.0...v3.0.0) (2020-02-09)

### Features

- change to named transform ([dcd0c9b](https://gitlab.com/html-validate/html-validate-vue/commit/dcd0c9bd8dacab0d72af08c05165390d2ac70bde))
- drop html-validate@1 compatibility ([3ee6d6a](https://gitlab.com/html-validate/html-validate-vue/commit/3ee6d6a150de91b78d1fa14fbc1a85bef52ff360))
- expose `auto` as more explicit autodetection transformer ([facd6df](https://gitlab.com/html-validate/html-validate-vue/commit/facd6dfae6451fe0412458ab9ca1a0ecdf01ab40))
- expose `html` transform to apply hooks only ([ff476c4](https://gitlab.com/html-validate/html-validate-vue/commit/ff476c4aa33740ec7dc7732e7c0dacd47e87c68e))
- expose `js` and `sfc` as named transformers ([c14536b](https://gitlab.com/html-validate/html-validate-vue/commit/c14536b8681f9ffcd17264c20430f1bf087b61b3))

### BREAKING CHANGES

- Previously it was possible to load this as a regular
  transformer using html-validate 1 but the `peerDependency` has required version
  2 for a while. The plugin also includes a runtime check for version 2. All uses
  must now update to html-validate 2 and load this as a plugin.

# [2.6.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.5.1...v2.6.0) (2020-02-05)

### Features

- handle `<component>` ([e792d9b](https://gitlab.com/html-validate/html-validate-vue/commit/e792d9b4cb36fa864852a376da0f585c0c8c2597))

## [2.5.1](https://gitlab.com/html-validate/html-validate-vue/compare/v2.5.0...v2.5.1) (2020-01-26)

### Bug Fixes

- **deps:** update dependency html-validate to v2.11.0 ([ee854bb](https://gitlab.com/html-validate/html-validate-vue/commit/ee854bb333120342120b39bd56ab624bb436301a))

# [2.5.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.4.0...v2.5.0) (2020-01-21)

### Features

- **elements:** support `<keep-alive>` ([7c6520b](https://gitlab.com/html-validate/html-validate-vue/commit/7c6520b7cb919dabf596015c193597444cbe5aee))
- **elements:** support `<router-link>` and `<router-view>` ([d1d3bf6](https://gitlab.com/html-validate/html-validate-vue/commit/d1d3bf67743a25345eed755878fc7bf651e54db8))

# [2.4.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.4...v2.4.0) (2020-01-15)

### Features

- allow selfclose tags by default ([9731077](https://gitlab.com/html-validate/html-validate-vue/commit/9731077c2cb083de1ed64807030d5b577a8b5b48))
- validate library version ([5562708](https://gitlab.com/html-validate/html-validate-vue/commit/5562708983e6e166b4272a35eb7d69ede2a2e915)), closes [html-validate#56](https://gitlab.com/html-validate/issues/56)

## [2.3.4](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.3...v2.3.4) (2019-12-27)

## [2.3.3](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.2...v2.3.3) (2019-12-27)

## [2.3.2](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.1...v2.3.2) (2019-12-27)

## [2.3.1](https://gitlab.com/html-validate/html-validate-vue/compare/v2.3.0...v2.3.1) (2019-12-27)

### Bug Fixes

- backwards compatibility with html-validate@1 ([36b613a](https://gitlab.com/html-validate/html-validate-vue/commit/36b613ab3a37a24a6d62973c9c7c9e682216c816)), closes [html-validate/html-validate#64](https://gitlab.com/html-validate/html-validate/issues/64)

# [2.3.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.2.1...v2.3.0) (2019-12-16)

### Features

- handle slot metadata ([9104f60](https://gitlab.com/html-validate/html-validate-vue/commit/9104f6025dfcef1f73d632536676219dcb76adf6))

## [2.2.1](https://gitlab.com/html-validate/html-validate-vue/compare/v2.2.0...v2.2.1) (2019-12-08)

### Bug Fixes

- add preamble length to offset ([239e0a7](https://gitlab.com/html-validate/html-validate-vue/commit/239e0a7bc58ffc3a8712a83da554d339f01b512f))

# [2.2.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.1.0...v2.2.0) (2019-12-02)

### Features

- set `offset` property ([dfb7b32](https://gitlab.com/html-validate/html-validate-vue/commit/dfb7b32d2ab6e14d32b1e1c325357a8597b5e275))

# [2.1.0](https://gitlab.com/html-validate/html-validate-vue/compare/v2.0.1...v2.1.0) (2019-11-30)

### Features

- reconfigure `attr-case` by default ([f5da29c](https://gitlab.com/html-validate/html-validate-vue/commit/f5da29c4ce48287f08fc7004a0a571083770bad3))

## [2.0.1](https://gitlab.com/html-validate/html-validate-vue/compare/v2.0.0...v2.0.1) (2019-11-28)

### Bug Fixes

- handle slots with dashes ([b3fa646](https://gitlab.com/html-validate/html-validate-vue/commit/b3fa6467f502f3206cdc1c608f41ff2c521b0ed8))

# [2.0.0](https://gitlab.com/html-validate/html-validate-vue/compare/v1.4.0...v2.0.0) (2019-11-24)

### Features

- add `.htmlvalidate.json` for easier testing during dev ([80bea40](https://gitlab.com/html-validate/html-validate-vue/commit/80bea40d6c31b2ccbab28a8959c43398e9fd8dde))
- add recommended config ([a476f4c](https://gitlab.com/html-validate/html-validate-vue/commit/a476f4c02e25df1470199609eee74a04c5e83877))
- add required-slots rule ([9e8a100](https://gitlab.com/html-validate/html-validate-vue/commit/9e8a1007e40140fe59872ef64c3e67046bb6bb59))
- convert to plugin ([fc380e1](https://gitlab.com/html-validate/html-validate-vue/commit/fc380e1881f5117a0a0a0913a1b4edcab3d8538f))
- validate available slots ([c69a0e4](https://gitlab.com/html-validate/html-validate-vue/commit/c69a0e4e393ae5e6d277fcbd561bbb73e35b72f3))

### BREAKING CHANGES

- the plugin must be included using `plugin: ["html-validate-vue"]` and not just as a transformer.

# [1.4.0](https://gitlab.com/html-validate/html-validate-vue/compare/v1.3.2...v1.4.0) (2019-11-17)

### Features

- html-validate@2.0.0 compatibility ([17d9cbb](https://gitlab.com/html-validate/html-validate-vue/commit/17d9cbb60a46db32a97732db085da758c9da4a89))

## [1.3.2](https://gitlab.com/html-validate/html-validate-vue/compare/v1.3.1...v1.3.2) (2019-11-13)

### Bug Fixes

- add `<transition>` element ([74b8f3a](https://gitlab.com/html-validate/html-validate-vue/commit/74b8f3a8c8fcc02860a0767684d30fa1982774d1)), closes [#2](https://gitlab.com/html-validate/html-validate-vue/issues/2)

## [1.3.1](https://gitlab.com/html-validate/html-validate-vue/compare/v1.3.0...v1.3.1) (2019-10-08)

### Bug Fixes

- **transform:** handle `v-html` and `<slot>` as dynamic text ([16d5ac7](https://gitlab.com/html-validate/html-validate-vue/commit/16d5ac7)), closes [#1](https://gitlab.com/html-validate/html-validate-vue/issues/1)

# [1.3.0](https://gitlab.com/html-validate/html-validate-vue/compare/v1.2.0...v1.3.0) (2019-09-23)

### Features

- **elements:** add `elements.json` with `<slot>` declaration ([03025da](https://gitlab.com/html-validate/html-validate-vue/commit/03025da))

## 1.2.0 (2019-02-24)

- Improved attribute handling
- Rewritten in typescript

## 1.1.0 (2019-02-19)

- Handle `:foo` and `v-bind:foo` as dynamic attributes.
- Bump dependencies

## 1.0.7 (2019-02-19)

- Migrate project to gitlab.com

## 1.0.6 (2018-12-16)

- Bump html-validate to 0.16.1

## 1.0.5 (2018-11-07)

- Bump html-vlidate to 0.14.2
