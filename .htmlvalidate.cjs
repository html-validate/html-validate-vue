const Plugin = require("./dist/cjs/index.cjs");

module.exports = {
	plugins: [Plugin],
	extends: ["html-validate:recommended", "html-validate-vue:recommended"],
	elements: ["html5", "./test/custom.json"],
	transform: {
		"^.*\\.vue$": "html-validate-vue",
		"^.*\\.spec.ts$": "html-validate-vue:html",
	},
};
